#!/usr/bin/perl
# vim: set filetype=perl sw=4 tabstop=4 expandtab smartindent: #

=head1 NAME

  akismet_sites - plugin to get the number of API calls from sites

=head1 AUTHOR AND COPYRIGHT

  Copyright 2023 Framasoft

=head1 HOWTO CONFIGURE AND USE :

=over

=item - /etc/munin/plugin-conf.d/akismet_sites

     [akismet_sites]
     env.keys foo bar

=item - env.keys

  This is the Akismet API keys

=item - /etc/munin/plugins

     ln -s akismet_sites /etc/munin/plugins/akismet_sites

=item - restart Munin node

     service munin-node restart

=back

=head1 DEPENDENCIES

  You will need the Perl distribution Mojolicious

  Although it is certainly available in your GNU/Linux packages, it recommended to install it trough the cpan command:

     cpan Mojolicious

  You’ll also need IO::Socket::SSL (may require libssl-dev to install)

     cpan IO::Socket::SSL

=head1 LICENSE

  This program is free software: you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation, either version 3 of the License, or any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

use warnings;
use strict;
use Munin::Plugin;
use Mojo::UserAgent;
use Mojo::JSON qw(decode_json);

my $PLUGIN_NAME = 'akismet_sites';

munin_exit_fail() unless (defined($ENV{keys}));

my @keys = split(' ', $ENV{keys});

my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
my $month = sprintf("%i-%02i", $year + 1900, ++$mon);
my %sites;
my $ua = Mojo::UserAgent->new();
for my $key (@keys) {
    my $u   = Mojo::URL->new('https://rest.akismet.com/1.2/key-sites')->query(api_key => $key);
    my $tx  = $ua->get($u);
    my $res = $tx->res;
    if ($res->is_success) {
        my $r = decode_json($res->body);
        for my $site (@{$r->{$month}}) {
            $sites{$site->{site}} = (defined($sites{$site->{site}})) ? $sites{$site->{site}} + $site->{api_calls} : $site->{api_calls};
        }
    } else {
        munin_exit_fail();
    }
}

##### config
if( (defined $ARGV[0]) && ($ARGV[0] eq 'config') ) {
    print "multigraph akismet_sites\n";
    print "graph_title Akismet API calls per site\n";
    print "graph_vlabel API calls\n";
    print "graph_args --lower-limit 0\n";
    print "graph_category akismet\n";
    print "graph_total Total\n";
    print "graph_info This graph shows the use of Akismet API key\n";
    my $i = 0;
    for my $site (sort(keys %sites)) {
        my $draw = ($i++) ? 'AREASTACK' : 'AREA';
        my $site_clean = $site;
        $site_clean =~ s/\./_/g;
        print "$site_clean.label $site\n";
        print "$site_clean.draw $draw\n";
    }
    for my $site (sort(keys %sites)) {
        print "multigraph akismet_sites.$site\n";
        print "graph_title Akismet API calls from $site\n";
        print "graph_vlabel API calls\n";
        print "graph_args --lower-limit 0\n";
        print "graph_category akismet\n";
        my $site_clean = $site;
        $site_clean =~ s/\./_/g;
        print "$site_clean.label $site\n";
        print "$site_clean.draw AREA\n";
    }
    munin_exit_done();
}

##### fetch
for my $site (sort(keys %sites)) {
    my $site_clean = $site;
    $site_clean =~ s/\./_/g;
    print $site_clean, '.value ', $sites{$site}, "\n";
}
for my $site (sort(keys %sites)) {
    print "multigraph akismet_sites.$site\n";
    my $site_clean = $site;
    $site_clean =~ s/\./_/g;
    print $site_clean, '.value ', $sites{$site}, "\n";
}
munin_exit_done();

#
##
### INTERNALS FONCTIONS
###############################################################################
sub munin_exit_done {
    munin_exit(0);
} ## sub munin_exit_done

sub munin_exit_fail {
    munin_exit(1);
} ## sub munin_exit_fail

sub munin_exit {
    my $exitcode = shift;
    exit($exitcode) if(defined $exitcode);
    exit(1);
} ## sub munin_exit
