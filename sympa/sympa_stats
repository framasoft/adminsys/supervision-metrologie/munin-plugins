#!/usr/bin/perl -w
use strict;

#
# Plugin to monitor the number of mails processed by sympa
# Heavily based on exim_mailstats
#
# Usage: copy or link into /etc/munin/plugins/
#
# Parameters:
#
#     config   (required)
#     autoconf (optional - used by munin-config)
#
# Config variables:
#
#     logfile       - Where to find the syslog-information
#
# Magic markers (optional - used by munin-config and some installation
# scripts):
#
#%# family=auto
#%# capabilities=autoconf
#
# Example of /etc/munin/plugin-conf.d/munin-node configuration
# [sympa_stats]
# user root
#
# root is needed to read syslog

my $statefile = "/var/lib/munin/plugin-state/plugin-sympa_stats.state";

my $pos = undef;
my $oldFiles = 0;
my $senderNotAllowed = 0;
my $keyToEditors = 0;
my $msgAccepted = 0;
my $msgArchived = 0;
$pos = 0;
my $logfile = $ENV{'logfile'} || '/var/log/syslog';
my $rotlogfile = '';


if ($ARGV[0] and $ARGV[0] eq "autoconf") {
   if(! -e $logfile) {
        print "no (logfile does not exist)\n";
        exit(1);
    }
    if (-r $logfile) {
        print "yes\n";
        exit 0;
    } else {
        print "no (logfile not readable)\n";
    }

    exit 1;
}

if (-f "$logfile.0") {
    $rotlogfile = $logfile . ".0";
} elsif (-f "$logfile.1") {
    $rotlogfile = $logfile . ".1";
} elsif (-f "$logfile.01") {
    $rotlogfile = $logfile . ".01";
}


if ($ARGV[0] and $ARGV[0] eq "config") {
    print "graph_title Statistics of sympa\n";
    print "graph_args --base 1000 -l 0\n";
    print "graph_vlabel messages/\${graph_period}\n";
    print "graph_scale  no\n";
    print "graph_category sympa\n";

    print "oldFiles.label old Files\n";
    print "oldFiles.type DERIVE\n";
    print "oldFiles.min 0\n";

    print "senderNotAllowed.label rejected\n";
    print "senderNotAllowed.type DERIVE\n";
    print "senderNotAllowed.min 0\n";

    print "keyToEditors.label keyToEditors\n";
    print "keyToEditors.type DERIVE\n";
    print "keyToEditors.min 0\n";

    print "msgAccepted.label msgAccepted\n";
    print "msgAccepted.type DERIVE\n";
    print "msgAccepted.min 0\n";

    print "msgArchived.label msgArchived\n";
    print "msgArchived.type DERIVE\n";
    print "msgArchived.min 0\n";


    exit 0;
}

if (! -f $logfile and ! -f $rotlogfile) {
    print "oldFiles.value U\n";
    print "senderNotAllowed.value U\n";
    print "keyToEditors.value U\n";
    print "msgAccepted.value U\n";
    print "msgArchived.value U\n";
    exit 0;
}

if (-f "$statefile") {
    open (my $file, '<', "$statefile") or exit 4;
    my $in = <$file>;
    if ($in =~ /^(\d+):(\d+):(\d+):(\d+):(\d+):(\d+):(\d+)/) {
        ($pos, $oldFiles, $senderNotAllowed, $keyToEditors, $msgAccepted, $msgArchived) = ($1, $2, $3, $4, $5, $6, $7);
    }
    close $file;
}

my $startsize = (stat $logfile)[7];

if (!defined $pos) {
    # Initial run.
    $pos = $startsize;
}

if ($startsize < $pos) {
    # Log rotated
    parseLogfile ($rotlogfile, $pos, (stat $rotlogfile)[7]);
    $pos = 0;
}

parseLogfile ($logfile, $pos, $startsize);
$pos = $startsize;
print "oldFiles.value $oldFiles\n";
print "senderNotAllowed.value $senderNotAllowed\n";
print "keyToEditors.value $keyToEditors\n";
print "msgAccepted.value $msgAccepted\n";
print "msgArchived.value $msgArchived\n";

if (-l $statefile) {
    die("$statefile is a symbolic link, refusing to touch it.");
}

open (my $out, '>', "$statefile") or exit 4;
print $out "$pos:$oldFiles:$senderNotAllowed:$keyToEditors:$msgAccepted:$msgArchived\n";
close $out;

sub parseLogfile {
    my ($fname, $start, $stop) = @_;
    open (my $LOGFILE, '<', $fname) or exit 3;
    seek ($LOGFILE, $start, 0) or exit 2;
    while (tell ($LOGFILE) < $stop) {
        my $line =<LOGFILE>;

        chomp ($line);

        if ($line=~/Deleting old file /) {
            $oldFiles++;
        } elsif ($line=~/rejected\(.*\) because sender not allowed/) {
            $senderNotAllowed++;
        } elsif ($line=~/Key \S+ of message \S+ for list \S+ from \S+ sent to editors /) {
            $keyToEditors++;
        } elsif ($line=~/Message \S+ for \S+ from \S+ accepted /) {
            $msgAccepted++;
        } elsif ($line=~/Archiving \S+ for list/) {
            $msgArchived++;
        }
    }
    close($LOGFILE);
}

# vim:syntax=perl
