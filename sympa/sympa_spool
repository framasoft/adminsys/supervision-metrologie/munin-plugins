#!/bin/bash
#
# Script to monitor disk usage for given mountpoints.
#
# Parameters understood:
#
#       config   (required)
#       autoconf (optional - used by munin-config)
#
# Magic markers (optional - used by munin-config and installation
# scripts):
#
#%# family=auto
#%# capabilities=autoconf
. "$MUNIN_LIBDIR/plugins/plugin.sh"

SYMPA_SPOOL=${sympa_spool:-/home/sympa/spool}
QUEUES=${queues:-auth bounce digest distribute expire moderation msg outgoing subscribe task tmp topic msg/bad distribute/bad}

# If we have parameters to the script
case $1 in
    autoconf)
        if [ -d "$SYMPA_SPOOL" ] && [ -r "$SYMPA_SPOOL" ]; then
            echo yes
            exit 0
        else
            echo "no (spool file not readable)"
            exit 1
        fi
        ;;
    config)
        echo 'graph_title Data in sympa-spool'
        echo 'graph_category sympa'
        for i in $QUEUES
        do
            echo "$i.draw LINE2" | tr "/" "_"
            echo -n "$i.label" | tr "/" "_"; echo " $i"
            if [ "$i" = "msg" ]; then
                echo "$i.warning 40"
                echo "$i.critical 60"
            fi
        done
        exit 0
        ;;
esac

# Else we show the results
for i in $QUEUES
do
    COUNT=$(/usr/bin/find "${SYMPA_SPOOL}/${i}" -maxdepth 1 -type f | wc -l | tr -d " ")
    echo "$i.value $COUNT" | tr "/" "_"
done
