#!/usr/bin/perl
# vim: set filetype=perl sw=4 tabstop=4 expandtab smartindent: #

=head1 NAME

  sympa_users - Used to track how many lists are opened on your Sympa server

=head1 AUTHOR AND COPYRIGHT

  Copyright 2016 Luc Didry <luc AT framasoft.org>

=head1 HOWTO CONFIGURE AND USE :

=over

=item - /etc/munin/plugin-conf.d/sympa_users

     [sympa_users]
     user postgres
     env.db_name sympa

=item - /etc/munin/plugins

     ln -s sympa_users /etc/munin/plugins/sympa_users

=item - restart Munin node

     service munin-node restart

=back

=head1 DEPENDENCIES

  You will need the Perl distribution Mojo::Pg

  Although it is certainly available in your GNU/Linux packages, it recommended to install it trough the cpan command:

     cpan Mojo::Pg

=head1 LICENSE

  This program is free software: you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation, either version 3 of the License, or any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

use warnings;
use strict;
use Munin::Plugin;
use Mojo::Pg;

my $PLUGIN_NAME = 'sympa_users';

my $db = $ENV{db_name} // 'sympa';
my $pg = Mojo::Pg->new("postgresql://postgres@/$db");

##### config
if (defined($ARGV[0]) && $ARGV[0] eq 'config') {
    print "graph_title Sympa users\n";
    print "graph_vlabel Number users of on Sympa\n";
    print "graph_args --lower-limit 0\n";
    print "graph_category sympa\n";
    print "graph_info This graph shows some stats about the number of users on your Sympa server\n";
    print "users.label Number of users\n";
    print "users.draw LINE\n";
    print "avg.label Avg nb of subscribers/list\n";
    print "avg.draw LINE\n";
    print "min.label Min nb of subscribers/list\n";
    print "min.draw LINE\n";
    print "max.label Max nb of subscribers/list\n";
    print "max.draw LINE\n";
    munin_exit_done();
}

##### fetch
print "users.value ".$pg->db->query('SELECT count(email_user) FROM user_table;')->hash->{count}."\n";
my $result = $pg->db->query('SELECT COUNT(user_subscriber) FROM subscriber_table GROUP BY list_subscriber;')->hashes;
my $lists  = $result->size;
my $users  = 0;
my $max    = 0;
my $min;
$result->each(
    sub {
        my ($e, $num) = @_;
        $max    = $e->{count} if ($e->{count} > $max);
        $min    = $e->{count} if (!defined($min) || $e->{count} < $min);
        $users += $e->{count};
    }
);
print "avg.value ".sprintf("%.2f", $users / $lists)."\n" if $lists;
print "avg.value 0\n" unless $lists;

print "min.value $min\n";
print "max.value $max\n";

munin_exit_done();

#
##
### INTERNALS FONCTIONS
###############################################################################
sub munin_exit_done {
    munin_exit(0);
} ## sub munin_exit_done

sub munin_exit_fail {
    munin_exit(1);
} ## sub munin_exit_fail

sub munin_exit {
    my $exitcode = shift;
    exit($exitcode) if(defined $exitcode);
    exit(1);
} ## sub munin_exit
