#!/usr/bin/perl
# vim: set filetype=perl sw=4 tabstop=4 expandtab smartindent: #

=head1 NAME

Pads restart - Count how many times there's a "RESTART" in Etherpad logs

=head1 AUTHOR

Contributed by Luc Didry

=head1 LICENSE

GPLv2

=head1 MAGIC MARKERS

 #%# family=auto
 #%# capabilities=autoconf

=cut

use warnings;
use strict;
use Munin::Plugin;

my $PLUGIN_NAME = 'pads_restart';

munin_exit_fail() unless (defined($ENV{pads}));

my @pads = split(' ', $ENV{pads});

if( (defined $ARGV[0]) && ($ARGV[0] eq 'autoconf') ) {
    print "no\n";
    munin_exit_done();
}

##### config
if( (defined $ARGV[0]) && ($ARGV[0] eq 'config') ) {
    print "multigraph pads_restart\n";
    print "graph_title Pads restart\n";
    print "graph_vlabel Number of RESTART\n";
    print "graph_args --lower-limit 0\n";
    print "graph_category etherpad\n";
    print "graph_total Total\n";
    print "graph_info Show how many times there's a \"RESTART\" in Etherpad logs\n";
    my $i = 0;
    for my $pad (@pads) {
        my $draw = ($i++) ? 'AREASTACK' : 'AREA';
        print "$pad.label $pad\n";
        print "$pad.draw $draw\n";
    }
    for my $pad (@pads) {
        print "multigraph pads_restart.$pad\n";
        print "graph_title Pad RESTARTs for ".$pad."\n";
        print "graph_vlabel Number of RESTART on ".$pad."\n";
        print "graph_args --lower-limit 0\n";
        print "graph_category etherpad\n";
        print "$pad.label $pad\n";
        print "$pad.draw AREA\n";
        print "$pad.warning 4\n";
        print "$pad.critical 8\n";
    }
    munin_exit_done();

}

##### fetch
my %cache;
for my $pad (@pads) {
    $cache{$pad} = `grep -c RESTART /var/log/framapad/$pad.framapad.log`;
    print "$pad.value $cache{$pad}";
}
for my $pad (@pads) {
    print "multigraph pads_restart.$pad\n";
    print "$pad.value $cache{$pad}";
}
munin_exit_done();

#
##
### INTERNALS FONCTIONS
###############################################################################
sub munin_exit_done {
    munin_exit(0);
} ## sub munin_exit_done

sub munin_exit_fail {
    munin_exit(1);
} ## sub munin_exit_fail

sub munin_exit {
    my $exitcode = shift;
    exit($exitcode) if(defined $exitcode);
    exit(1);
} ## sub munin_exit
