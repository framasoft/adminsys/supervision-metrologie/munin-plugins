#!/usr/bin/perl
# vim: set filetype=perl sw=4 tabstop=4 expandtab smartindent: #

=head1 NAME

gitlab_runner_jobs - Count how many jobs your Gitlab runners have received

=head1 CONFIGURATION

[gitlab_runner_jobs]
user root

=head1 AUTHOR

Contributed by Luc Didry

=head1 LICENSE

GPLv2

=cut

use warnings;
use strict;
use Munin::Plugin;

my $PLUGIN_NAME = 'gitlab_runner_jobs';

if( (defined $ARGV[0]) && ($ARGV[0] eq 'autoconf') ) {
    print "no\n";
    munin_exit_done();
}

##### config
if( (defined $ARGV[0]) && ($ARGV[0] eq 'config') ) {
    print "graph_title Gitlab runners jobs\n";
    print "graph_vlabel Number of restarts\n";
    print "graph_args --lower-limit 0\n";
    print "graph_category gitlab\n";
    print "graph_info Show how many jobs your Gitlab runners have received\n";
    print "jobs.label Jobs\n";
    print "jobs.draw AREA\n";
    munin_exit_done();
}

##### fetch
print "jobs.value ".`grep -c "Checking for jobs... received" /var/log/syslog`;
munin_exit_done();

#
##
### INTERNALS FONCTIONS
###############################################################################
sub munin_exit_done {
    munin_exit(0);
} ## sub munin_exit_done

sub munin_exit_fail {
    munin_exit(1);
} ## sub munin_exit_fail

sub munin_exit {
    my $exitcode = shift;
    exit($exitcode) if(defined $exitcode);
    exit(1);
} ## sub munin_exit
