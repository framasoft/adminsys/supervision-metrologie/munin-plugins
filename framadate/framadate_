#!/usr/bin/perl
# vim: set filetype=perl sw=4 tabstop=4 expandtab smartindent: #

=head1 NAME

  Framadate_ — get some stats about a framadate instance

=head1 AUTHOR AND COPYRIGHT

  Copyright 2022 Luc Didry <luc AT framasoft.org>

=head1 HOWTO CONFIGURE AND USE :

=over

=item - /etc/munin/plugin-conf.d/framadate

     [framadate_*]
     user root
     env.dbname framadate # default to framadate

=item - /etc/munin/plugins

     ln -s framadate_ /etc/munin/plugins/framadate_comments
     ln -s framadate_ /etc/munin/plugins/framadate_polls
     ln -s framadate_ /etc/munin/plugins/framadate_votes

=item - restart Munin node

     service munin-node restart

=back

=head1 DEPENDENCIES

  You will need the Perl distribution Mojo::mysql:

     cpan Mojo::mysql

=head1 LICENSE

  This program is free software: you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation, either version 3 of the License, or any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

use warnings;
use strict;
use Munin::Plugin;
use Mojo::mysql;
use Storable;
use POSIX;

my $metric= $Munin::Plugin::me;
my $PLUGIN_NAME = $metric;
$metric =~ s/framadate_//;
$metric =~ s/s$//;

my $dbname = $ENV{'dbname'} || 'framadate';
my $mysql = Mojo::mysql->strict_mode(sprintf 'mysql:///%s', $dbname);

my $cachefile = $Munin::Plugin::pluginstatedir.'/cache_'.$PLUGIN_NAME;

##### config
if( (defined $ARGV[0]) && ($ARGV[0] eq 'config') ) {
    printf "graph_title Framadate %ss stats\n", $metric;
    printf "graph_vlabel %ss number\n", ucfirst($metric);
    print "graph_args --lower-limit 0\n";
    print "graph_category framadate\n";
    print "graph_total Total\n";
    printf "graph_info This graph shows statistics about %ss on your framadate instance\n", $metric;
    printf "%ss.label %ss\n", $metric, ucfirst($metric);
    printf "%ss.draw AREA\n", $metric;
    munin_exit_done();
}

my $to_cache = {};

if (-f $cachefile) {
    my $cache = retrieve($cachefile);
    printf "%ss.value %d\n", $metric, $cache->{sprintf '%ss.value', $metric};
    unless (-f $cachefile.'-lock') {
        my $pid = fork();
        if (not $pid) {
            POSIX::setsid();
            fork and exit;
            open my $lockfile, '>', $cachefile.'-lock';
            close $lockfile;

            my $result = $mysql->db->query(sprintf('SELECT count(*) AS count FROM fd_%s;', $metric))->hashes->first;
            $to_cache->{sprintf '%ss.value', $metric} = $result->{count};

            store $to_cache, $cachefile;
            unlink $cachefile.'-lock';
        } else {
            waitpid $pid, 0;
        }
    }
} else {
    ##### fetch
    my $result = $mysql->db->query(sprintf('SELECT count(*) AS count FROM fd_%s;', $metric))->hashes->first;
    printf "%ss.value %d\n", $metric, $result->{count};
    $to_cache->{sprintf '%ss.value', $metric} = $result->{count};

    store $to_cache, $cachefile;
}

munin_exit_done();

#
##
### INTERNALS FONCTIONS
###############################################################################
sub munin_exit_done {
    munin_exit(0);
} ## sub munin_exit_done

sub munin_exit_fail {
    munin_exit(1);
} ## sub munin_exit_fail

sub munin_exit {
    my $exitcode = shift;
    exit($exitcode) if(defined $exitcode);
    exit(1);
} ## sub munin_exit
