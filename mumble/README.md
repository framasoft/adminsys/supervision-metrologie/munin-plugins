Récupéré de https://github.com/Natenom/munin-plugins/tree/master/murmur

Nécessite l’installation de `zeroc-ice-slice` et `python-zeroc-ice` (sur Debian).

Il faut aussi que la conf mumble contienne ça (y a juste à décommenter, normalement) :
```
ice="tcp -h 127.0.0.1 -p 6502"
```
