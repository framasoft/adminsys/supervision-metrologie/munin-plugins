#!/bin/bash
# -*- bash -*-

: <<=cut

=head1 NAME

lokas_past_jobs - Plugin to show some stats about past jobs of a lokas server.

=head1 DEPENDENCIES

You will need jq and curl.

=head1 INSTALLATION

ln -s lokas_past_jobs /etc/munin/plugins/lokas_past_jobs

=head1 CONFIGURATION

    [lokas_*]
    env.url https://lokas.example.org/api/v1/transcription/jobs/stats

=head1 AUTHOR

Contributed by Luc Didry, for Framasoft

=head1 LICENSE

GPLv2

=cut

. "$MUNIN_LIBDIR/plugins/plugin.sh"

plugin_name=$(basename "$0")

if [[ -z $url ]]; then
    echo "You need to set env.url." >&2
    exit 1
fi

if [ "$1" = "config" ]; then
    cat <<EOF
graph_title Lokas past jobs
graph_args --lower-limit 0
graph_category lokas
completed.label Active jobs
completed.draw LINE
failed.label Waiting jobs
failed.draw LINE
EOF
    exit 0
fi

stats=$(curl -s "$url")
cat <<EOF
completed.value $(echo "$stats" | jq -r .completed)
failed.value $(echo "$stats" | jq -r .failed)
EOF

exit 0
