#!/usr/bin/perl
# vim: set filetype=perl sw=4 tabstop=4 expandtab smartindent: #

=head1 NAME

Calc lite restart - Count how many times there's a "Started" in Ethercalc log

=head1 AUTHOR

Contributed by Luc Didry

=head1 LICENSE

GPLv2

=head1 MAGIC MARKERS

 #%# family=auto
 #%# capabilities=autoconf

=cut

use warnings;
use strict;
use Munin::Plugin;

my $PLUGIN_NAME = 'calc_lite_restart';

if( (defined $ARGV[0]) && ($ARGV[0] eq 'autoconf') ) {
    print "no\n";
    munin_exit_done();
}

##### config
if( (defined $ARGV[0]) && ($ARGV[0] eq 'config') ) {
    print "graph_title Calc restart\n";
    print "graph_vlabel Number of restarts\n";
    print "graph_args --lower-limit 0\n";
    print "graph_category ethercalc\n";
    print "graph_info Show how many times there's a \"Started\" in Ethercalc log\n";
    print "calc_lite.label Framacalc lite\n";
    print "calc_lite.draw AREA\n";
    print "calc_lite.warning 4\n";
    print "calc_lite.critical 8\n";
    munin_exit_done();
}

##### fetch
print "calc_lite.value ".`grep -c Started /var/log/framacalc/lite.framacalc.log`;
munin_exit_done();

#
##
### INTERNALS FONCTIONS
###############################################################################
sub munin_exit_done {
    munin_exit(0);
} ## sub munin_exit_done

sub munin_exit_fail {
    munin_exit(1);
} ## sub munin_exit_fail

sub munin_exit {
    my $exitcode = shift;
    exit($exitcode) if(defined $exitcode);
    exit(1);
} ## sub munin_exit
