## Dependencies

You need to activate statistics on [Videobridge](https://github.com/jitsi/jitsi-videobridge/blob/master/doc/statistics.md) and install `libmojolicious-perl` (on Debian).

## Warning

Those plugins won’t work with jitsi-videobridge >= 2.0:
- jitsi_total_channels
- jitsi_audiochannels
